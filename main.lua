local composer = require( "composer" )
local scene = composer.newScene()

local levelmenu = require( "levelmenu" )

menu = levelmenu.createMenu({
	rows = 4,
	columns = 3,

	paddingLeft = 10,
	paddingRight = 10,
	padding = 20,

	--levelCount = 30,
	-- or
	levels = {
		{name = 'Worms', stars = 0},
		{name = 'Ants', stars = 1},
		{name = 'Spiders', stars = 2},
		{name = 'Fish', stars = 3},
		{name = 'Frog'},
		{name = 'aaa'},
		{name = 'bbb'},
		{name = 'ccc'},
	},

	appendLevelNumber = false,

	fontSize = 14,
	fontColor = { 64 / 255, 169 / 255, 71 / 255 },

	imageLocked = 'images/locked.png',
	imageUnlocked = 'images/unlocked.png',
	imageStar = 'images/star.png',

	unlockedTo = 5,

	eventListener = function ( event )
		print ('event name', event.name)
		print ('event phase', event.phase)
		print ('level number', event.target.levelNumber)
		print ('level name', event.target.levelName)
		
		return true
	end
})
