# Corona Level Menu widget

## How to use it

```lua
local levelmenu = require( "levelmenu" )

menu = levelmenu.createMenu({
	rows = 4,
	columns = 3,

	levelCount = 24,
	
	fontSize = 14,

	imageLocked = 'locked.png',
	imageUnlocked = 'unlocked.png',
	imageStar = 'star.png',

	unlockedTo = 5,

	eventListener = function ( event )
		-- touch listener
		return true
	end
})

sceneGroup:insert( menu )
```

## Configuration options

### Container layout options

* **left** *(default 0)* - left position of the menu
* **top** *(default 0)* - top position of the menu
* **width** *(default screen width)* - width of the menu
* **height** *(default screen height)* - height of the menu
* **rows** *(default 4)* - number of the rows on one page.
* **columns** *(default 3)* - number of the columns on one page.
* **padding** *(default 0)* - padding of the menu
* **paddingTop** *(default 0)* - top padding of the menu. This value override the **padding** value.
* **paddingBottom** *(default 0)* - bottom padding of the menu. This value override the **padding** value.
* **paddingLeft** *(default 0)* - left padding of the menu. This value override the **padding** value.
* **paddingRight** *(default 0)* - right padding of the menu. This value override the **padding** value.

### Menu item options

* **itemWidth** *(default 80)* - width of the menu item
* **itemHeight** *(default 80)* - height of the menu item
* **levels** - array of the levels definition
* **levelCount** - if the **levels** is not defined, **levelCount** will enable simple layout. It means the name of the level will be **LEVEL X** where **X** is auto increment value.
* **appendLevelNumber** *(default true)* - You can use custom names for levels if **levels** array is defined. You can disable level number in this case.
* **drawLevelNameOnLocked** *(default false)* - if **drawLevelNameOnLocked** is set to **true** then level name for locked level will be displayed.
* **unlockedTo** *(default 1)* - specifies which levels are unlocked
* **eventListener** - handler for the touch event
* **imageUnlocked** - path to the image for unlocked levels
* **imageLocked** - path to the image for locked levels
* **imageStar** - path to the image for stars
* **font** *(default native.systemFont)* - font of the level name
* **fontSize** *(default 13)* - font size for the level name
* **fontColor** - font color for the level name
* **starsOffset** *(default 50)* - offset of the stars
* **starSpace** *(default 8)* - space between the stars if the stars count is greather than one.

## Level definition

For the more complex configuration use array of the level definition.
```lua
local levelmenu = require( "levelmenu" )

menu = levelmenu.createMenu({
	rows = 4,
	columns = 3,

	levels = {
		{name = 'Worms', stars = 0},
		{name = 'Ants', stars = 1},
		{name = 'Spiders', stars = 2},
		{name = 'Fish', stars = 3},
		{name = 'Frog'}
	},
	
	fontSize = 14,

	imageLocked = 'locked.png',
	imageUnlocked = 'unlocked.png',
	imageStar = 'star.png',

	unlockedTo = 5,

	eventListener = function ( event )
		-- touch listener
		return true
	end
})

sceneGroup:insert( menu )
```

Every item contains:

* **name** *(default "LEVEL")* - name of the level. Level number will be appended if **appendLevelNumber** is **true**.
* **stars** *(default 0)* - sets how many stars will be displayed.

## Event handling

Unlocked items will generate **touch** event. 
You can access standard event.target property with two additional properties.

* event.target.levelNumber
* event.target.levelName

```lua
menu = levelmenu.createMenu({
	-- ... rest of the settings

	eventListener = function ( event )
		print ('event name', event.name)
		print ('event phase', event.phase)
		print ('level number', event.target.levelNumber)
		print ('level name', event.target.levelName)

		-- process event here...
		
		return true
	end
})
```

## How it looks

Watch the video on [youtube](http://youtu.be/Lfjk9ee3cuM).

### Simple layout
![IMAGE](raw/master/images/simple_layout.png)

### Custom level definition
![IMAGE](raw/master/images/levels_definition.png)
